// NOTE: Assertions have been autogenerated by utils/update_cc_test_checks.py
// RUN: %clang_cc1 -triple aarch64-linux-gnu -target-feature +morello \
// RUN:    -emit-llvm -o - %s -disable-O0-optnone | \
// RUN:  opt -S -mem2reg | \
// RUN:  FileCheck  %s

#include <stdarg.h>

// CHECK-LABEL: @foo(
// CHECK-NEXT:  entry:
// CHECK-NEXT:    [[ARGS:%.*]] = alloca [[STRUCT___VA_LIST:%.*]], align 8
// CHECK-NEXT:    call void @llvm.va_start.p0(ptr [[ARGS]])
// CHECK-NEXT:    [[GR_OFFS_P:%.*]] = getelementptr inbounds [[STRUCT___VA_LIST]], ptr [[ARGS]], i32 0, i32 3
// CHECK-NEXT:    [[GR_OFFS:%.*]] = load i32, ptr [[GR_OFFS_P]], align 8
// CHECK-NEXT:    [[TMP0:%.*]] = icmp sge i32 [[GR_OFFS]], 0
// CHECK-NEXT:    br i1 [[TMP0]], label [[VAARG_ON_STACK:%.*]], label [[VAARG_MAYBE_REG:%.*]]
// CHECK:       vaarg.maybe_reg:
// CHECK-NEXT:    [[NEW_REG_OFFS:%.*]] = add i32 [[GR_OFFS]], 8
// CHECK-NEXT:    store i32 [[NEW_REG_OFFS]], ptr [[GR_OFFS_P]], align 8
// CHECK-NEXT:    [[INREG:%.*]] = icmp sle i32 [[NEW_REG_OFFS]], 0
// CHECK-NEXT:    br i1 [[INREG]], label [[VAARG_IN_REG:%.*]], label [[VAARG_ON_STACK]]
// CHECK:       vaarg.in_reg:
// CHECK-NEXT:    [[REG_TOP_P:%.*]] = getelementptr inbounds [[STRUCT___VA_LIST]], ptr [[ARGS]], i32 0, i32 1
// CHECK-NEXT:    [[REG_TOP:%.*]] = load ptr, ptr [[REG_TOP_P]], align 8
// CHECK-NEXT:    [[TMP1:%.*]] = getelementptr inbounds i8, ptr [[REG_TOP]], i32 [[GR_OFFS]]
// CHECK-NEXT:    br label [[VAARG_END:%.*]]
// CHECK:       vaarg.on_stack:
// CHECK-NEXT:    [[STACK_P:%.*]] = getelementptr inbounds [[STRUCT___VA_LIST]], ptr [[ARGS]], i32 0, i32 0
// CHECK-NEXT:    [[STACK:%.*]] = load ptr, ptr [[STACK_P]], align 8
// CHECK-NEXT:    [[NEW_STACK:%.*]] = getelementptr inbounds i8, ptr [[STACK]], i64 8
// CHECK-NEXT:    store ptr [[NEW_STACK]], ptr [[STACK_P]], align 8
// CHECK-NEXT:    br label [[VAARG_END]]
// CHECK:       vaarg.end:
// CHECK-NEXT:    [[VAARGS_ADDR:%.*]] = phi ptr [ [[TMP1]], [[VAARG_IN_REG]] ], [ [[STACK]], [[VAARG_ON_STACK]] ]
// CHECK-NEXT:    [[VAARG_ADDR:%.*]] = load ptr, ptr [[VAARGS_ADDR]], align 8
// CHECK-NEXT:    [[TMP2:%.*]] = load ptr addrspace(200), ptr [[VAARG_ADDR]], align 16
// CHECK-NEXT:    call void @llvm.va_end.p0(ptr [[ARGS]])
// CHECK-NEXT:    ret ptr addrspace(200) [[TMP2]]
//
__intcap_t foo(int count, ...) {
    va_list args;
    va_start(args, count);
    __intcap_t cap = va_arg(args, __intcap_t);
    va_end(args);
    return cap;
}

// CHECK-LABEL: @bar(
// CHECK-NEXT:  entry:
// CHECK-NEXT:    [[INDIRECT_ARG_TEMP:%.*]] = alloca ptr addrspace(200), align 16
// CHECK-NEXT:    store ptr addrspace(200) null, ptr [[INDIRECT_ARG_TEMP]], align 16
// CHECK-NEXT:    [[CALL:%.*]] = call ptr addrspace(200) (i32, ...) @foo(i32 noundef 4, ptr noundef [[INDIRECT_ARG_TEMP]])
// CHECK-NEXT:    ret void
//
void bar(void) {
    __intcap_t cap = 0;
    foo(4, cap);
}

// CHECK-LABEL: @baz(
// CHECK-NEXT:  entry:
// CHECK-NEXT:    [[ARGS:%.*]] = alloca [[STRUCT___VA_LIST:%.*]], align 8
// CHECK-NEXT:    call void @llvm.va_start.p0(ptr [[ARGS]])
// CHECK-NEXT:    [[GR_OFFS_P:%.*]] = getelementptr inbounds [[STRUCT___VA_LIST]], ptr [[ARGS]], i32 0, i32 3
// CHECK-NEXT:    [[GR_OFFS:%.*]] = load i32, ptr [[GR_OFFS_P]], align 8
// CHECK-NEXT:    [[TMP0:%.*]] = icmp sge i32 [[GR_OFFS]], 0
// CHECK-NEXT:    br i1 [[TMP0]], label [[VAARG_ON_STACK:%.*]], label [[VAARG_MAYBE_REG:%.*]]
// CHECK:       vaarg.maybe_reg:
// CHECK-NEXT:    [[NEW_REG_OFFS:%.*]] = add i32 [[GR_OFFS]], 8
// CHECK-NEXT:    store i32 [[NEW_REG_OFFS]], ptr [[GR_OFFS_P]], align 8
// CHECK-NEXT:    [[INREG:%.*]] = icmp sle i32 [[NEW_REG_OFFS]], 0
// CHECK-NEXT:    br i1 [[INREG]], label [[VAARG_IN_REG:%.*]], label [[VAARG_ON_STACK]]
// CHECK:       vaarg.in_reg:
// CHECK-NEXT:    [[REG_TOP_P:%.*]] = getelementptr inbounds [[STRUCT___VA_LIST]], ptr [[ARGS]], i32 0, i32 1
// CHECK-NEXT:    [[REG_TOP:%.*]] = load ptr, ptr [[REG_TOP_P]], align 8
// CHECK-NEXT:    [[TMP1:%.*]] = getelementptr inbounds i8, ptr [[REG_TOP]], i32 [[GR_OFFS]]
// CHECK-NEXT:    br label [[VAARG_END:%.*]]
// CHECK:       vaarg.on_stack:
// CHECK-NEXT:    [[STACK_P:%.*]] = getelementptr inbounds [[STRUCT___VA_LIST]], ptr [[ARGS]], i32 0, i32 0
// CHECK-NEXT:    [[STACK:%.*]] = load ptr, ptr [[STACK_P]], align 8
// CHECK-NEXT:    [[NEW_STACK:%.*]] = getelementptr inbounds i8, ptr [[STACK]], i64 8
// CHECK-NEXT:    store ptr [[NEW_STACK]], ptr [[STACK_P]], align 8
// CHECK-NEXT:    br label [[VAARG_END]]
// CHECK:       vaarg.end:
// CHECK-NEXT:    [[VAARGS_ADDR:%.*]] = phi ptr [ [[TMP1]], [[VAARG_IN_REG]] ], [ [[STACK]], [[VAARG_ON_STACK]] ]
// CHECK-NEXT:    [[VAARG_ADDR:%.*]] = load ptr, ptr [[VAARGS_ADDR]], align 8
// CHECK-NEXT:    [[TMP2:%.*]] = load ptr addrspace(200), ptr [[VAARG_ADDR]], align 16
// CHECK-NEXT:    call void @llvm.va_end.p0(ptr [[ARGS]])
// CHECK-NEXT:    ret ptr addrspace(200) [[TMP2]]
//
void *__capability baz(int count, ...) {
    va_list args;
    va_start(args, count);
    void *__capability cap = va_arg(args, void *__capability);
    va_end(args);
    return cap;
}

int x;

// CHECK-LABEL: @bif(
// CHECK-NEXT:  entry:
// CHECK-NEXT:    [[INDIRECT_ARG_TEMP:%.*]] = alloca ptr addrspace(200), align 16
// CHECK-NEXT:    store ptr addrspace(200) addrspacecast (ptr @x to ptr addrspace(200)), ptr [[INDIRECT_ARG_TEMP]], align 16
// CHECK-NEXT:    [[CALL:%.*]] = call ptr addrspace(200) (i32, ...) @baz(i32 noundef 4, ptr noundef [[INDIRECT_ARG_TEMP]])
// CHECK-NEXT:    ret void
//
void bif(void) {
    void *__capability cap = &x;
    baz(4, cap);
}

struct str {
  int *__capability x;
  int y;
};

// CHECK-LABEL: @bat(
// CHECK-NEXT:  entry:
// CHECK-NEXT:    [[RETVAL:%.*]] = alloca [[STRUCT_STR:%.*]], align 16
// CHECK-NEXT:    [[ARGS:%.*]] = alloca [[STRUCT___VA_LIST:%.*]], align 8
// CHECK-NEXT:    call void @llvm.va_start.p0(ptr [[ARGS]])
// CHECK-NEXT:    [[GR_OFFS_P:%.*]] = getelementptr inbounds [[STRUCT___VA_LIST]], ptr [[ARGS]], i32 0, i32 3
// CHECK-NEXT:    [[GR_OFFS:%.*]] = load i32, ptr [[GR_OFFS_P]], align 8
// CHECK-NEXT:    [[TMP0:%.*]] = icmp sge i32 [[GR_OFFS]], 0
// CHECK-NEXT:    br i1 [[TMP0]], label [[VAARG_ON_STACK:%.*]], label [[VAARG_MAYBE_REG:%.*]]
// CHECK:       vaarg.maybe_reg:
// CHECK-NEXT:    [[NEW_REG_OFFS:%.*]] = add i32 [[GR_OFFS]], 8
// CHECK-NEXT:    store i32 [[NEW_REG_OFFS]], ptr [[GR_OFFS_P]], align 8
// CHECK-NEXT:    [[INREG:%.*]] = icmp sle i32 [[NEW_REG_OFFS]], 0
// CHECK-NEXT:    br i1 [[INREG]], label [[VAARG_IN_REG:%.*]], label [[VAARG_ON_STACK]]
// CHECK:       vaarg.in_reg:
// CHECK-NEXT:    [[REG_TOP_P:%.*]] = getelementptr inbounds [[STRUCT___VA_LIST]], ptr [[ARGS]], i32 0, i32 1
// CHECK-NEXT:    [[REG_TOP:%.*]] = load ptr, ptr [[REG_TOP_P]], align 8
// CHECK-NEXT:    [[TMP1:%.*]] = getelementptr inbounds i8, ptr [[REG_TOP]], i32 [[GR_OFFS]]
// CHECK-NEXT:    br label [[VAARG_END:%.*]]
// CHECK:       vaarg.on_stack:
// CHECK-NEXT:    [[STACK_P:%.*]] = getelementptr inbounds [[STRUCT___VA_LIST]], ptr [[ARGS]], i32 0, i32 0
// CHECK-NEXT:    [[STACK:%.*]] = load ptr, ptr [[STACK_P]], align 8
// CHECK-NEXT:    [[NEW_STACK:%.*]] = getelementptr inbounds i8, ptr [[STACK]], i64 8
// CHECK-NEXT:    store ptr [[NEW_STACK]], ptr [[STACK_P]], align 8
// CHECK-NEXT:    br label [[VAARG_END]]
// CHECK:       vaarg.end:
// CHECK-NEXT:    [[VAARGS_ADDR:%.*]] = phi ptr [ [[TMP1]], [[VAARG_IN_REG]] ], [ [[STACK]], [[VAARG_ON_STACK]] ]
// CHECK-NEXT:    [[VAARG_ADDR:%.*]] = load ptr, ptr [[VAARGS_ADDR]], align 8
// CHECK-NEXT:    call void @llvm.memcpy.p0.p0.i64(ptr align 16 [[RETVAL]], ptr align 16 [[VAARG_ADDR]], i64 32, i1 false)
// CHECK-NEXT:    call void @llvm.va_end.p0(ptr [[ARGS]])
// CHECK-NEXT:    [[TMP2:%.*]] = load { ptr addrspace(200), i64 }, ptr [[RETVAL]], align 16
// CHECK-NEXT:    ret { ptr addrspace(200), i64 } [[TMP2]]
//
struct str bat(int count, ...) {
    va_list args;
    va_start(args, count);
    struct str cap_struct = va_arg(args, struct str);
    va_end(args);
    return cap_struct;
}

// CHECK-LABEL: @fiz(
// CHECK-NEXT:  entry:
// CHECK-NEXT:    [[CAP_STRUCT:%.*]] = alloca [[STRUCT_STR:%.*]], align 16
// CHECK-NEXT:    [[BYVAL_TEMP:%.*]] = alloca [[STRUCT_STR]], align 16
// CHECK-NEXT:    [[COERCE:%.*]] = alloca [[STRUCT_STR]], align 16
// CHECK-NEXT:    [[X:%.*]] = getelementptr inbounds [[STRUCT_STR]], ptr [[CAP_STRUCT]], i32 0, i32 0
// CHECK-NEXT:    store ptr addrspace(200) addrspacecast (ptr @x to ptr addrspace(200)), ptr [[X]], align 16
// CHECK-NEXT:    [[Y:%.*]] = getelementptr inbounds [[STRUCT_STR]], ptr [[CAP_STRUCT]], i32 0, i32 1
// CHECK-NEXT:    store i32 0, ptr [[Y]], align 16
// CHECK-NEXT:    call void @llvm.memcpy.p0.p0.i64(ptr align 16 [[BYVAL_TEMP]], ptr align 16 [[CAP_STRUCT]], i64 32, i1 false)
// CHECK-NEXT:    [[CALL:%.*]] = call { ptr addrspace(200), i64 } (i32, ...) @bat(i32 noundef 4, ptr noundef [[BYVAL_TEMP]])
// CHECK-NEXT:    [[TMP0:%.*]] = getelementptr inbounds { ptr addrspace(200), i64 }, ptr [[COERCE]], i32 0, i32 0
// CHECK-NEXT:    [[TMP1:%.*]] = extractvalue { ptr addrspace(200), i64 } [[CALL]], 0
// CHECK-NEXT:    store ptr addrspace(200) [[TMP1]], ptr [[TMP0]], align 16
// CHECK-NEXT:    [[TMP2:%.*]] = getelementptr inbounds { ptr addrspace(200), i64 }, ptr [[COERCE]], i32 0, i32 1
// CHECK-NEXT:    [[TMP3:%.*]] = extractvalue { ptr addrspace(200), i64 } [[CALL]], 1
// CHECK-NEXT:    store i64 [[TMP3]], ptr [[TMP2]], align 16
// CHECK-NEXT:    ret void
//
void fiz(void) {
    struct str cap_struct;
    cap_struct.x = &x;
    cap_struct.y = 0;
    bat(4, cap_struct);
}
