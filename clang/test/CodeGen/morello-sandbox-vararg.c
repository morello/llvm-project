// NOTE: Assertions have been autogenerated by utils/update_cc_test_checks.py
// REQUIRES: asserts
// RUN: %clang %s -target aarch64-none-elf -march=morello+c64 -mabi=purecap -o - -S -emit-llvm \
// RUN:   | FileCheck %s

// Tests for handling of variable argument lists in the sandbox mode.

#include <stdarg.h>

// Check that calls to the llvm.va_start, llvm.va_copy and llvm.va_end
// intrinsics are made with pointers to address space 200.
// CHECK-LABEL: @testVaAddressSpace(
// CHECK-NEXT:  entry:
// CHECK-NEXT:    [[A_ADDR:%.*]] = alloca i32, align 4, addrspace(200)
// CHECK-NEXT:    [[VL:%.*]] = alloca ptr addrspace(200), align 16, addrspace(200)
// CHECK-NEXT:    [[VL2:%.*]] = alloca ptr addrspace(200), align 16, addrspace(200)
// CHECK-NEXT:    [[RES:%.*]] = alloca i32, align 4, addrspace(200)
// CHECK-NEXT:    store i32 [[A:%.*]], ptr addrspace(200) [[A_ADDR]], align 4
// CHECK-NEXT:    call void @llvm.va_start.p200(ptr addrspace(200) [[VL]])
// CHECK-NEXT:    [[STACK:%.*]] = load ptr addrspace(200), ptr addrspace(200) [[VL]], align 16
// CHECK-NEXT:    [[NEW_STACK:%.*]] = getelementptr inbounds i8, ptr addrspace(200) [[STACK]], i64 16
// CHECK-NEXT:    store ptr addrspace(200) [[NEW_STACK]], ptr addrspace(200) [[VL]], align 16
// CHECK-NEXT:    [[TMP0:%.*]] = load i32, ptr addrspace(200) [[STACK]], align 16
// CHECK-NEXT:    store i32 [[TMP0]], ptr addrspace(200) [[RES]], align 4
// CHECK-NEXT:    call void @llvm.va_copy.p200.p200(ptr addrspace(200) [[VL2]], ptr addrspace(200) [[VL]])
// CHECK-NEXT:    call void @llvm.va_end.p200(ptr addrspace(200) [[VL2]])
// CHECK-NEXT:    call void @llvm.va_end.p200(ptr addrspace(200) [[VL]])
// CHECK-NEXT:    [[TMP1:%.*]] = load i32, ptr addrspace(200) [[RES]], align 4
// CHECK-NEXT:    ret i32 [[TMP1]]
//
int testVaAddressSpace(int a, ...) {
  va_list vl, vl2;
  int res;

  va_start(vl, a);

  res = va_arg(vl, int);

  va_copy(vl2, vl);

  va_end(vl2);

  va_end(vl);

  return res;
}

// CHECK-LABEL: @testIntVaArg(
// CHECK-NEXT:  entry:
// CHECK-NEXT:    [[GINT_ADDR:%.*]] = alloca ptr addrspace(200), align 16, addrspace(200)
// CHECK-NEXT:    [[B_ADDR:%.*]] = alloca i32, align 4, addrspace(200)
// CHECK-NEXT:    [[VL:%.*]] = alloca ptr addrspace(200), align 16, addrspace(200)
// CHECK-NEXT:    store ptr addrspace(200) [[GINT:%.*]], ptr addrspace(200) [[GINT_ADDR]], align 16
// CHECK-NEXT:    store i32 [[B:%.*]], ptr addrspace(200) [[B_ADDR]], align 4
// CHECK-NEXT:    call void @llvm.va_start.p200(ptr addrspace(200) [[VL]])
// CHECK-NEXT:    [[STACK:%.*]] = load ptr addrspace(200), ptr addrspace(200) [[VL]], align 16
// CHECK-NEXT:    [[NEW_STACK:%.*]] = getelementptr inbounds i8, ptr addrspace(200) [[STACK]], i64 16
// CHECK-NEXT:    store ptr addrspace(200) [[NEW_STACK]], ptr addrspace(200) [[VL]], align 16
// CHECK-NEXT:    [[TMP0:%.*]] = load i32, ptr addrspace(200) [[STACK]], align 16
// CHECK-NEXT:    [[TMP1:%.*]] = load ptr addrspace(200), ptr addrspace(200) [[GINT_ADDR]], align 16
// CHECK-NEXT:    [[TMP2:%.*]] = load i32, ptr addrspace(200) [[TMP1]], align 4
// CHECK-NEXT:    [[ADD:%.*]] = add nsw i32 [[TMP2]], [[TMP0]]
// CHECK-NEXT:    store i32 [[ADD]], ptr addrspace(200) [[TMP1]], align 4
// CHECK-NEXT:    call void @llvm.va_end.p200(ptr addrspace(200) [[VL]])
// CHECK-NEXT:    ret void
//
void testIntVaArg(int *gInt, int b, ...) {
  va_list vl;
  va_start(vl, b);
  *gInt += va_arg(vl, int);
  va_end(vl);
}

// CHECK-LABEL: @testDoubleVaArg(
// CHECK-NEXT:  entry:
// CHECK-NEXT:    [[GDOUBLE_ADDR:%.*]] = alloca ptr addrspace(200), align 16, addrspace(200)
// CHECK-NEXT:    [[B_ADDR:%.*]] = alloca i32, align 4, addrspace(200)
// CHECK-NEXT:    [[VL:%.*]] = alloca ptr addrspace(200), align 16, addrspace(200)
// CHECK-NEXT:    store ptr addrspace(200) [[GDOUBLE:%.*]], ptr addrspace(200) [[GDOUBLE_ADDR]], align 16
// CHECK-NEXT:    store i32 [[B:%.*]], ptr addrspace(200) [[B_ADDR]], align 4
// CHECK-NEXT:    call void @llvm.va_start.p200(ptr addrspace(200) [[VL]])
// CHECK-NEXT:    [[STACK:%.*]] = load ptr addrspace(200), ptr addrspace(200) [[VL]], align 16
// CHECK-NEXT:    [[NEW_STACK:%.*]] = getelementptr inbounds i8, ptr addrspace(200) [[STACK]], i64 16
// CHECK-NEXT:    store ptr addrspace(200) [[NEW_STACK]], ptr addrspace(200) [[VL]], align 16
// CHECK-NEXT:    [[TMP0:%.*]] = load double, ptr addrspace(200) [[STACK]], align 16
// CHECK-NEXT:    [[TMP1:%.*]] = load ptr addrspace(200), ptr addrspace(200) [[GDOUBLE_ADDR]], align 16
// CHECK-NEXT:    [[TMP2:%.*]] = load double, ptr addrspace(200) [[TMP1]], align 8
// CHECK-NEXT:    [[ADD:%.*]] = fadd double [[TMP2]], [[TMP0]]
// CHECK-NEXT:    store double [[ADD]], ptr addrspace(200) [[TMP1]], align 8
// CHECK-NEXT:    call void @llvm.va_end.p200(ptr addrspace(200) [[VL]])
// CHECK-NEXT:    ret void
//
void testDoubleVaArg(double *gDouble, int b, ...) {
  va_list vl;
  va_start(vl, b);
  *gDouble += va_arg(vl, double);
  va_end(vl);
}

// CHECK-LABEL: @testCapVaArg(
// CHECK-NEXT:  entry:
// CHECK-NEXT:    [[B_ADDR:%.*]] = alloca i32, align 4, addrspace(200)
// CHECK-NEXT:    [[VL:%.*]] = alloca ptr addrspace(200), align 16, addrspace(200)
// CHECK-NEXT:    [[RET:%.*]] = alloca ptr addrspace(200), align 16, addrspace(200)
// CHECK-NEXT:    store i32 [[B:%.*]], ptr addrspace(200) [[B_ADDR]], align 4
// CHECK-NEXT:    call void @llvm.va_start.p200(ptr addrspace(200) [[VL]])
// CHECK-NEXT:    [[STACK:%.*]] = load ptr addrspace(200), ptr addrspace(200) [[VL]], align 16
// CHECK-NEXT:    [[NEW_STACK:%.*]] = getelementptr inbounds i8, ptr addrspace(200) [[STACK]], i64 16
// CHECK-NEXT:    store ptr addrspace(200) [[NEW_STACK]], ptr addrspace(200) [[VL]], align 16
// CHECK-NEXT:    [[TMP0:%.*]] = load ptr addrspace(200), ptr addrspace(200) [[STACK]], align 16
// CHECK-NEXT:    store ptr addrspace(200) [[TMP0]], ptr addrspace(200) [[RET]], align 16
// CHECK-NEXT:    call void @llvm.va_end.p200(ptr addrspace(200) [[VL]])
// CHECK-NEXT:    [[TMP1:%.*]] = load ptr addrspace(200), ptr addrspace(200) [[RET]], align 16
// CHECK-NEXT:    ret ptr addrspace(200) [[TMP1]]
//
void *testCapVaArg(int b, ...) {
  va_list vl;
  void *ret;
  va_start(vl, b);
  ret = va_arg(vl, void *);
  va_end(vl);
  return ret;
}

struct capstruct {
  int *a;
  int *b;
};

void call_capstruct(struct capstruct);

// CHECK-LABEL: @checkStructCapVa(
// CHECK-NEXT:  entry:
// CHECK-NEXT:    [[Z_ADDR:%.*]] = alloca i32, align 4, addrspace(200)
// CHECK-NEXT:    [[ARG:%.*]] = alloca [[STRUCT_CAPSTRUCT:%.*]], align 16, addrspace(200)
// CHECK-NEXT:    [[AP:%.*]] = alloca ptr addrspace(200), align 16, addrspace(200)
// CHECK-NEXT:    store i32 [[Z:%.*]], ptr addrspace(200) [[Z_ADDR]], align 4
// CHECK-NEXT:    call void @llvm.va_start.p200(ptr addrspace(200) [[AP]])
// CHECK-NEXT:    [[STACK:%.*]] = load ptr addrspace(200), ptr addrspace(200) [[AP]], align 16
// CHECK-NEXT:    [[NEW_STACK:%.*]] = getelementptr inbounds i8, ptr addrspace(200) [[STACK]], i64 16
// CHECK-NEXT:    store ptr addrspace(200) [[NEW_STACK]], ptr addrspace(200) [[AP]], align 16
// CHECK-NEXT:    [[VAARG_ADDR:%.*]] = load ptr addrspace(200), ptr addrspace(200) [[STACK]], align 16
// CHECK-NEXT:    call void @llvm.memcpy.p200.p200.i64(ptr addrspace(200) align 16 [[ARG]], ptr addrspace(200) align 16 [[VAARG_ADDR]], i64 32, i1 false)
// CHECK-NEXT:    [[TMP0:%.*]] = load { ptr addrspace(200), ptr addrspace(200) }, ptr addrspace(200) [[ARG]], align 16
// CHECK-NEXT:    call void @call_capstruct({ ptr addrspace(200), ptr addrspace(200) } [[TMP0]])
// CHECK-NEXT:    call void @llvm.va_end.p200(ptr addrspace(200) [[AP]])
// CHECK-NEXT:    ret void
//
void checkStructCapVa (int z, ...)
{
  struct capstruct arg;
  va_list ap;
  va_start(ap, z);
  arg = va_arg (ap, struct capstruct);
  call_capstruct(arg);
  va_end (ap);
}

// Capability loads from the register spill area are done with alignment 16.
// CHECK-LABEL: @foo(
// CHECK-NEXT:  entry:
// CHECK-NEXT:    [[BAR_ADDR:%.*]] = alloca ptr addrspace(200), align 16, addrspace(200)
// CHECK-NEXT:    [[BAZ:%.*]] = alloca ptr addrspace(200), align 16, addrspace(200)
// CHECK-NEXT:    store ptr addrspace(200) [[BAR:%.*]], ptr addrspace(200) [[BAR_ADDR]], align 16
// CHECK-NEXT:    [[STACK:%.*]] = load ptr addrspace(200), ptr addrspace(200) [[BAR_ADDR]], align 16
// CHECK-NEXT:    [[NEW_STACK:%.*]] = getelementptr inbounds i8, ptr addrspace(200) [[STACK]], i64 16
// CHECK-NEXT:    store ptr addrspace(200) [[NEW_STACK]], ptr addrspace(200) [[BAR_ADDR]], align 16
// CHECK-NEXT:    [[TMP0:%.*]] = load ptr addrspace(200), ptr addrspace(200) [[STACK]], align 16
// CHECK-NEXT:    store ptr addrspace(200) [[TMP0]], ptr addrspace(200) [[BAZ]], align 16
// CHECK-NEXT:    ret void
//
void foo(__builtin_va_list bar) {
  char *baz = __builtin_va_arg(bar, char *);
}
